import React from "react";
import Sidebar from "./Sidebar";
import List from "./List";
import Navbar from "./Navbar";

import "./Userlist.css";

function Userlist() {



    return (
        <>

            <Navbar /> 

            <div className="dashboard">

                <div className="flex1">
                    <Sidebar />
                </div>

                <div className="flex2">
                    <h2>User</h2>
                    <List />
                </div>



            </div>



        </>
    )
}

export default Userlist;